const express = require("express");

const router = express.Router();

const courseMiddleware = require("../middlewares/course.middleware");
const courseController = require("../controllers/course.controller");

router.get("/courses", courseMiddleware.getAllCourseMiddleware, courseController.getAllCourse);

router.post("/courses", courseMiddleware.createCourseMiddleware, courseController.createCourse);

router.get("/courses/:courseid", courseMiddleware.getCourseByIDMiddleware, courseController.getCourseById);

router.put("/courses/:courseid", courseMiddleware.updateCourseMiddleware, courseController.updateCourseById);

router.delete("/courses/:courseid", courseMiddleware.deleteCourseMiddleware, courseController.deleteCourseById);

module.exports = router; // Tương tự export router

