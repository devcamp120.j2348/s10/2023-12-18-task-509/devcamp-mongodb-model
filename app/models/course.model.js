//B1: khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2: khai báo Schema
const Schema = mongoose.Schema

//B3: Tạo ra 1 schema theo thông tin cần quản lý
const courseSchema = new Schema({
    _id:mongoose.Types.ObjectId,
    title:{
        type:String,
        required:true,
        unique:true
    },
    description:{
        type:String,
        required:false
    },
    noStudent:{
        type:Number,
        default:0
    },
    reviews:[
        {
            type:mongoose.Types.ObjectId,
            ref:"review"
        }
    ]
}, { timestamps : true})

//B4: compile và export module
module.exports = mongoose.model("course", courseSchema)