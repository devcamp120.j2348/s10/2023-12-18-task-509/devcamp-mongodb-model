//B1: khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2: khai báo Schema
const Schema = mongoose.Schema

//B3: Tạo ra 1 schema theo thông tin cần quản lý
const reviewSchema = new Schema({
    _id:mongoose.Types.ObjectId,
    stars:{
        type:Number,
        default:0
    },
    note:{
        type:String,
        required:false
    }
}, { timestamps : true })

//B4: compile và export module
module.exports = mongoose.model("review", reviewSchema)